<?php

/**
 * @file
 * Webform module fieldset component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_summation() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'title_display' => 0,
      'description' => '',
      'description_above' => FALSE,
      'private' => FALSE,
      'summation_settings' => array(
        'numaric_fields' => array(
          'options' => array('One', 'Two', 'Three'),
        ),
        'field_max' => 1000,
        'field_min' => 0,
      ),
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_summation($component) {
  $form = array();
  $form['extra']['summation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Summation Field Settings'),
  );
  $form['extra']['summation_settings']['hide_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Field Title'),
    '#default_value' => isset($component['extra']['summation_settings']['hide_title']) ? $component['extra']['summation_settings']['hide_title'] : 0,
  );
  $form['extra']['summation_settings']['field_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Field Prefix'),
    '#description' => t('This prifix will be used for all numaric fields.'),
    '#default_value' => isset($component['extra']['summation_settings']['field_prefix']) ? $component['extra']['summation_settings']['field_prefix'] : '',
  );
  $form['extra']['summation_settings']['field_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Field Suffix'),
    '#description' => t('This suffix will be used for all numaric fields.'),
    '#default_value' => isset($component['extra']['summation_settings']['field_suffix']) ? $component['extra']['summation_settings']['field_suffix'] : '',
  );
  $form['extra']['summation_settings']['field_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum allowed value'),
    '#description' => t('Limit the input to a maximum value.'),
    '#default_value' => isset($component['extra']['summation_settings']['field_max']) ? $component['extra']['summation_settings']['field_max'] : '',
    '#attributes' => array('class' => array('clearfix'), ' type' => 'number'),
  );
  $form['extra']['summation_settings']['field_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum allowed value'),
    '#description' => t('Limit the input to a minimum value.'),
    '#default_value' => isset($component['extra']['summation_settings']['field_min']) ? $component['extra']['summation_settings']['field_min'] : '',
    '#attributes' => array('class' => array('clearfix'), ' type' => 'number'),
  );
  module_load_include('inc', 'webform', 'components/select');
  $form['extra']['summation_settings']['numaric_fields'] = array(
    '#type' => 'options',
    '#title' => t('Numeric Field Title'),
    '#multiple' => TRUE,
    '#default_value_allowed' => FALSE,
    '#options' => isset($component['extra']['summation_settings']['numaric_fields']['options']) ? $component['extra']['summation_settings']['numaric_fields']['options'] : array(),
  );
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  $submissions = webform_get_submissions(array('nid' => $component['nid']));
  if (!empty($submissions)) {
    $form['extra']['summation_settings']['numaric_fields']['#description'] = t('If his field has values in database. removing any field or updating any existing value will delete all related data from the database.');
  }
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_summation($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;
  // Include Angular to the page.
  drupal_add_js('https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js', 'external');

  // Create app name.
  $element_controller = str_replace(' ', '', $component['name']);
  $element_app = str_replace('-', '', $element_controller);
  // Create a unique id for the wrapper.
  $element_id = drupal_html_id('element-' . $element_controller);

  $element = array(
    '#type' => 'fieldset',
    '#title' => (isset($component['extra']['summation_settings']['hide_title']) && $component['extra']['summation_settings']['hide_title']) ? '' : $component['name'],
    '#weight' => $component['weight'],
    '#attributes' => array(
      'class' => array(
        'clearfix'
      ),
      'ng-app' => $element_app,
      'id' => $element_id
    ),
    '#pre_render' => array('webform_summation_prerender', 'webform_element_title_display'),
    '#translatable' => array('title'),
    '#tree' => TRUE,
  );
  if (!empty($component['extra']['summation_settings']['numaric_fields']['options'])) {
    $all_childs_str = '';

    // Render each field given in the settings.
    foreach ($component['extra']['summation_settings']['numaric_fields']['options'] as $key => $field) {
      $element_child_name = strtolower($field);
      $element_child_name = str_replace(' ', '_', $element_child_name);
      $element_child_name = str_replace('-', '_', $element_child_name);

      // Create angular expression for total.
      if ($key != count($component['extra']['summation_settings']['numaric_fields']['options']) - 1) {
        $all_childs_str .= 'summationElement' . $element_child_name . ' -- ';
      }
      else {
        $all_childs_str .= 'summationElement' . $element_child_name;
      }
      $element[$element_child_name] = array(
        '#type' => 'textfield',
        '#title' => t('@field', array('@field' => $field)),
        '#attributes' => array(
          'class' => array('clearfix'),
          'ng-model' => 'summationElement' . $element_child_name,
          ' type' => 'number'
        ),
        '#pre_render' => array('webform_element_title_display'),
        '#translatable' => array('title'),
        '#default_value' => isset($value[$element_child_name]) ? $value[$element_child_name] : '',
        '#field_prefix' => isset($component['extra']['summation_settings']['field_prefix']) ? $component['extra']['summation_settings']['field_prefix'] : '',
        '#field_suffix' => isset($component['extra']['summation_settings']['field_suffix']) ? $component['extra']['summation_settings']['field_suffix'] : '',
      );
      if (isset($component['extra']['summation_settings']['field_min'])) {
        $element[$element_child_name]['#attributes']['min'] = $component['extra']['summation_settings']['field_min'];
      }
      if (isset($component['extra']['summation_settings']['field_max'])) {
        $element[$element_child_name]['#attributes']['max'] = $component['extra']['summation_settings']['field_max'];
      }
      if (isset($value[$element_child_name])) {
        $element[$element_child_name]['#attributes']['ng-init'] = 'summationElement' . $element_child_name . ' = "' . $value[$element_child_name] . '"';
      }
    }
    // Element for the total.
    $element['total'] = array(
      '#type' => 'textfield',
      '#title' => t('Total'),
      '#disabled' => TRUE,
      '#value' => '{{ ' . $all_childs_str . ' }}',
      '#pre_render' => array('webform_element_title_display'),
      '#field_prefix' => isset($component['extra']['summation_settings']['field_prefix']) ? $component['extra']['summation_settings']['field_prefix'] : '',
      '#field_suffix' => isset($component['extra']['summation_settings']['field_suffix']) ? $component['extra']['summation_settings']['field_suffix'] : '',
      '#attributes' => array(' type' => 'number'),
    );

    $angular_controller = 'var app' . $element_app . ' = angular.module("' . $element_app . '", []);';

    // Code to ignore first element bootstrap because it is auto bootstraped.
    $summation_type_fields = 0;
    foreach ($node->webform['components'] as $key => $webform_component) {
      if ($webform_component['type'] == 'summation') {
        $summation_type_fields++;
        if ($summation_type_fields == 1) {
          $first_summation_element = $webform_component['form_key'];
        }
      }
    }
    if ($first_summation_element != $component['form_key']) {
      $angular_controller .= 'angular.bootstrap(document.getElementById("' . $element_id . '"), ["' . $element_app . '"]);';
    }

    // Add angular code at the footer of the page.
    drupal_add_js($angular_controller, array(
      'type' => 'inline',
      'scope' => 'footer',
    ));
  }
  return $element;
}

/**
 * Pre-render function to set a unique fieldset class name.
 */
function webform_summation_prerender($element) {
  $element['#attributes']['class'][] = 'webform-component--' . str_replace('_', '-', implode('--', array_slice($element['#parents'], 1)));
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_summation($component, $value, $format = 'html', $submission = array()) {
  if ($component['type'] == 'summation') {
    $element = array(
      '#title' => ($component['extra']['summation_settings']['hide_title']) ? '' : $component['name'],
      '#weight' => $component['weight'],
      '#theme' => 'webform_display_summation',
      '#theme_wrappers' => array('webform_element'),
      '#value' => isset($value) ? $value : '',
    );
  }
  return $element;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_summation($variables) {
  $element = $variables['element'];
  $extra = $element['#webform_component']['extra'];
  $output = '<table><tbody>';
  foreach ($extra['summation_settings']['numaric_fields']['options'] as $key => $value) {
    $output .= '<tr>';
    $output .= '<td>' . strip_tags($value) . '</td>';
    $output .= '<td style="text-align:right;">';
    $element_child_name = strtolower($value);
    $element_child_name = str_replace(' ', '_', $element_child_name);
    $element_child_name = str_replace('-', '_', $element_child_name);
    if (isset($element['#value'][$element_child_name])) {
      $output .= '<span class="field-prefix">' . $extra['summation_settings']['field_prefix'] . '</span>';
      $output .= $element['#value'][$element_child_name];
      $output .= '<span class="field-suffix">' . $extra['summation_settings']['field_suffix'] . '</span>';
    }
    else {
      $output .= '-';
    }
    $output .= '</td>';
    $output .= '</tr>';
    $all_values[] = isset($element['#value'][$element_child_name]) ? $element['#value'][$element_child_name] : 0;
  }
  $output .= '<tr><th style="border-top: 1px solid;">Total</th><th style="text-align:right; border-top: 1px solid;"><span class="field-prefix">' . $extra['summation_settings']['field_prefix'] . '</span>' . array_sum($all_values) . '<span class="field-suffix">' . $extra['summation_settings']['field_suffix'] . '</span></th></tr>';
  $output .= '</tbody></table>';
  return $output;
}

/**
 * Implements _form_builder_webform_form_builder_types_component().
 */
function _form_builder_webform_form_builder_types_summation() {
  $fields = array();

  $fields['summation'] = array(
    'title' => t('Summation'),
    'weight' => 20,
  );
  $component['name'] = t('Summation');
  $fields['summation']['default'] = _form_builder_webform_default('summation', array(), $component);

  return $fields;
}

/**
 * Implements _form_builder_webform_form_builder_map_component().
 */
function _form_builder_webform_form_builder_map_summation() {
  return array(
    'form_builder_type' => 'summation',
    'properties' => array(
      'summation_settings' => array(
        'form_parents' => array('extra', 'summation_settings'),
        'storage_parents' => array('extra', 'summation_settings'),
        'numaric_fields' => array(
          'form_parents' => array(
            'extra',
            'summation_settings',
            'numaric_fields'
          ),
          'storage_parents' => array('summation_settings', 'numaric_fields'),
        ),
        'field_prefix' => array(
          'form_parents' => array('extra', 'summation_settings', 'field_prefix'),
          'storage_parents' => array('summation_settings', 'field_prefix'),
        ),
        'field_suffix' => array(
          'form_parents' => array('extra', 'summation_settings', 'field_suffix'),
          'storage_parents' => array('summation_settings', 'field_suffix'),
        ),
        'hide_title' => array(
          'form_parents' => array('extra', 'summation_settings', 'hide_title'),
          'storage_parents' => array('summation_settings', 'hide_title'),
        ),
        'field_max' => array(
          'form_parents' => array('extra', 'summation_settings', 'field_max'),
          'storage_parents' => array('summation_settings', 'field_max'),
        ),
        'field_min' => array(
          'form_parents' => array('extra', 'summation_settings', 'field_min'),
          'storage_parents' => array('summation_settings', 'field_min'),
        ),
      ),
      'title' => array(
        'form_parents' => array('extra', 'title'),
        'storage_parents' => array('name'),
      ),
    ),
  );
}
